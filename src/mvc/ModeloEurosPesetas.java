/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

/**
 * Fichero: ModeloEurosPesetas.java
 *
 * @date 16-feb-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloEurosPesetas extends ModeloEuros { // Adaptador de clase
  public ModeloEurosPesetas() {
    super(166.386D);
  }

  public double eurosApesetas(double cantidad) {
    return eurosAmoneda(cantidad);
  }

  public double pesetasAeuros(double cantidad) {
    return monedaAeuros(cantidad);
  }
}
