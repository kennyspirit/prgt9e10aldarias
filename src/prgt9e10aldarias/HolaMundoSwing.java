package prgt9e10aldarias;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class HolaMundoSwing extends JFrame {

  public HolaMundoSwing() {
    super("Hola Mundo"); // Titulo  
    JLabel label = new JLabel("Hola Mundo");
    this.getContentPane().add(label);
    setSize(200, 100);
    setLocationRelativeTo(null); // Centrar 
    setVisible(true);
  }

  public static void main(String[] args) {
    new HolaMundoSwing();
  }
}
