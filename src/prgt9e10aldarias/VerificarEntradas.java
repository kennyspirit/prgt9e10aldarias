/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt9e10aldarias;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Fichero: VerificarEntradas.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 11-mar-2014
 */
public class VerificarEntradas extends JFrame {

  public VerificarEntradas() {
    JLabel jlabel1 = new JLabel("Numero:");
    JTextField jtextfield1 = new JTextField("");
    JLabel jl2 = new JLabel("Texto:");
    JTextField jtf2 = new JTextField("");

    getContentPane().add(jlabel1);
    getContentPane().add(jtextfield1);
    getContentPane().add(jl2);
    getContentPane().add(jtf2);
    setLayout(new GridLayout(2, 2));


    // Verificar la entrada.
    jtextfield1.setInputVerifier(new Verificador());


    WindowListener l = new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    };
    addWindowListener(l);
  }

  public static void main(String[] args) {
    Frame f = new VerificarEntradas();
    f.pack();
    f.setSize(200, 100);
    f.setLocationRelativeTo(null);
    f.setVisible(true);
  }
}

class Verificador extends InputVerifier {

  public boolean verify(JComponent input) {

    if (input instanceof JTextField) {
      String texto = ((JTextField) input).getText();
      try {
        // Si se puede convertir en entero, está bien
        Integer.parseInt(texto);
        return true;
      } catch (Exception e) {
        // Si no se ha podido convertir a entero, mostramos
        // una ventana de error y devolvemos false
        JOptionPane.showMessageDialog(input, "No es un número");
        return false;
      }
    }
    return true;


  }
}
