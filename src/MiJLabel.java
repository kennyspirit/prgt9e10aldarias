import javax.swing.*;
import java.awt.*;

public class MiJLabel {

   MiJLabel() {
      JFrame f = new JFrame();
      JPanel p = new JPanel();
      JLabel l = new JLabel();
      f.setTitle("Ejemplo JLabel");
      f.setLayout( new GridLayout() );
      f.add(p);
      f.setSize(230, 100);
      f.setVisible(true);
      p.add(l);
      l.setText("Texto del JLabel");
   }

  public static void main  
  (String args[]){
      new MiJLabel();
   }
}
