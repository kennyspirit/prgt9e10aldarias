/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swing06bFormulario;

/**
 * Fichero: ModeloCliente.java
 *
 * @date 03-abr-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class ModeloCliente {

  private String Id;
  private String Nombre;

  public ModeloCliente(String i, String n) {
    Id = i;
    Nombre = n;
  }

  public ModeloCliente() {
  }

  /**
   * @return the Id
   */
  public String getId() {
    return Id;
  }

  /**
   * @param Id the Id to set
   */
  public void setId(String Id) {
    this.Id = Id;
  }

  /**
   * @return the Nombre
   */
  public String getNombre() {
    return Nombre;
  }

  /**
   * @param Nombre the Nombre to set
   */
  public void setNombre(String Nombre) {
    this.Nombre = Nombre;
  }
}
