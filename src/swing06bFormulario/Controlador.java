/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swing06bFormulario;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Fichero: Controlador.java
 *
 * @date 03-abr-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Controlador implements ActionListener {

  VistaPantalla vista = null;
  ModeloCliente cliente = null;
  VistaFichero f = new VistaFichero();

  Controlador(VistaPantalla v, ModeloCliente m) {
    vista = v;
    cliente = m;

    vista.setVisible(true);
    primero();
    rellenavista(cliente);
    programaBotones();

  }

  private void programaBotones() {
    //Se añade las acciones a los controles del formulario vista
    vista.getjButtonAnterior().setActionCommand("Anterior");
    vista.getjButtonSiguiente().setActionCommand("Siguiente");
    //Se pone a escuchar las acciones del usuario
    vista.getjButtonAnterior().addActionListener(this);
    vista.getjButtonSiguiente().addActionListener(this);
  }

  private void rellenavista(ModeloCliente cliente) {
    vista.setjTextFieldId(cliente.getId());
    vista.setjTextFieldNombre(cliente.getNombre());
  }

  @Override
  public void actionPerformed(ActionEvent ae) {

    String comando = ae.getActionCommand();

    /* Acciones del formulario padre */
    switch (comando) {
      case "Anterior":
        anterior();
        rellenavista(cliente);
        break;
      case "Siguiente":
        siguiente();
        rellenavista(cliente);
        break;
    }
  }

  private void primero() {
    cliente = f.primero();
  }

  private void anterior() {
    int id;

    if (cliente == null) {
      return;
    }

    id = Integer.parseInt(cliente.getId());
    id--;

    if (id == 0) {
      return;
    }

    cliente = f.getCliente(String.valueOf(id));


  }

  private void siguiente() {
    int id;
    if (cliente == null) {
      return;
    }

    id = Integer.parseInt(cliente.getId());
    id++;
    cliente = f.getCliente(String.valueOf(id));
  }
}
